# Wardrobify
Designed and Created By
*Sorena Sawyer*

## Intended Market

Wardrobify has been created for people who have trouble remembering what items they have in their wardrobe! Easily manage your closet by keeping track of what you have and where it's located.

## Functionality

Maintain your closet by adding in your clothing items and keep track of where it's located. Whether you have a large or small wardrobe, you'll never have to worry about forgetting anything! 

##Installation

1. Clone the repository down to your local machine
2. CD into the new project directory
3. Run docker volume create postgres-data
4. Run docker compose build
5. Run docker compose up

