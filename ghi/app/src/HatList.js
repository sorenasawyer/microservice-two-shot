import React from "react";
import { NavLink } from "react-router-dom";

class ListHats extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hats: []
    };
  }

  async componentDidMount() {
    const url = ('http://localhost:8090/api/hats/');
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        this.setState({hats: data.hats});
        console.log(data)
      }
  }

  async handleDelete(event) {
    const id = event.target.value;
    const hatsUrl = `http://localhost:8090/api/hats/${id}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json'
      },
    };
    const hatResponse = await fetch(hatsUrl, fetchConfig);
    if (hatResponse.ok) {
      this.setState ({hats: this.state.hats.filter(hat => hat.id != id)})
      console.log(`hat ${id} successfully deleted`);
    } else {
      throw new Error("Response not ok");
    }
  }


  render () {
    return (
      <>
        <div className="container" p-10 m-10>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Style</th>
                <th>Location</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Image</th>
              </tr>
            </thead>
            <tbody>
              {this.state.hats.map(hat => {
                  return (
                    <tr key={hat.id}>
                    <td>{hat.style}</td>
                    <td>{hat.location}</td>
                    <td>{hat.fabric}</td>
                    <td>{hat.color}</td>
                    <td><img src={hat.image_url} className='img-thumbnail' width="100px" height="100px" /></td>
                    <td>
                    <button className="btn btn-outline-danger" onClick={this.handleDelete.bind(this)} value={hat.id}>Delete</button>
                    </td>
                    </tr>
                  );
              })}
            </tbody>
          </table>
        </div>
        <div className="d-grid gap-3">
          <button p-4 m-4 type="button" className="btn btn-outline-info btn sm">
            <NavLink to="/hats/new">Create New Hat</NavLink>
          </button>
		</div>
    </>
  );
};
}

export default ListHats;
